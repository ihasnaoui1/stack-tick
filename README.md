# stack-TICK
<br>
c'est la composition entre Telegraf, InfluxDB, Chronograf, Kapacitor proposé par InfluxData.<br>

* Influxdb : une Time Series Database (TSDB), pour la collecte des metriques
* Telegraf : un agent de récupération de métriques
* Chronograf : un système de visualisation de données.  Il va venir se connecter à notre base de données InfluxDB
* Kapacitor : gérer les alerts

Dans ce tuto je vais installer la stack  dans un machine linux sous format des conteneurs.<br>
Il faut installer Docker et Docker-compose.<br>

1. Clonage du repo

  ```
  git clone https://gitlab.com/ihasnaoui1/stack-tick.git
  cd stack-tick
  ```
  
<br><br>
2. Creation des volume et network : <br>
```
docker volume create influxdb-volume
docker volume create kapacitor-volume
docker network create monitoring
```

3. Configuration basede données Influxdb : 

```
docker run --rm \
  -e INFLUXDB_DB=telegraf -e INFLUXDB_ADMIN_ENABLED=true \
  -e INFLUXDB_ADMIN_USER=admin \
  -e INFLUXDB_ADMIN_PASSWORD=supersecretpassword \
  -e INFLUXDB_USER=telegraf -e INFLUXDB_USER_PASSWORD=secretpassword \
  -v influxdb-volume:/var/lib/influxdb \
  influxdb /init-influxdb.sh
  ```
<br>
Maintenant on va utiliser la configuration suivante : <br>

```
INFLUXDB_DB=telegraf
INFLUXDB_USER=telegraf
INFLUXDB_USER_PASSWORD=secretpassword
URL_influxdb= http://influxdb:8086
```
4. Lancer la stack :

```
docker-compose up -d
```

==> une fois le docker-compose s'exeucte sans erreur, on va verfier l'état des containers en tapant :
```
# docker ps

CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                        NAMES
28d068af99fd        chronograf          "/entrypoint.sh chro…"   6 seconds ago         2 seconds ago         0.0.0.0:8888->8888/tcp                       chronograf
a90c02654b1c        telegraf            "/entrypoint.sh tele…"   6 seconds ago         2 seconds ago          8092/udp, 8125/udp, 0.0.0.0:8094->8094/tcp   telegraf
c4962b996ff0        influxdb            "/entrypoint.sh infl…"   6 seconds ago         2 seconds ago          0.0.0.0:8086->8086/tcp                       influxdb
69313bb1614d        kapacitor           "/entrypoint.sh kapa…"   6 seconds ago         2 seconds ago          0.0.0.0:9092->9092/tcp                       kapacitor
```

5. Configuration du Chronograf <br>

Dans le navigateur, taper `http://localhost:8888` pour acceder à l'interface de chronograf et inserer la configuration vers influxdb et kapacitor comme suit : <br>




